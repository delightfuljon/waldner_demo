### A friendly message from Jon Bell

I went looking for something with Auto DevOps and found this. Also curious to know how the web IDE works!

Testing, testing, testing.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

1. [Turn on Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
1. [Learn more about Auto DevOps in this 10 minute video](https://www.youtube.com/watch?v=0Tc0YYBxqi4&feature=youtu.be)
1. Onward! 
